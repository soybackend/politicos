# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Votante.direccion'
        db.add_column(u'personas_votante', 'direccion',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Adding field 'Cabeza_de_grupo.direccion'
        db.add_column(u'personas_cabeza_de_grupo', 'direccion',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Adding field 'Lider.direccion'
        db.add_column(u'personas_lider', 'direccion',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Adding field 'Coordinador.direccion'
        db.add_column(u'personas_coordinador', 'direccion',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Votante.direccion'
        db.delete_column(u'personas_votante', 'direccion')

        # Deleting field 'Cabeza_de_grupo.direccion'
        db.delete_column(u'personas_cabeza_de_grupo', 'direccion')

        # Deleting field 'Lider.direccion'
        db.delete_column(u'personas_lider', 'direccion')

        # Deleting field 'Coordinador.direccion'
        db.delete_column(u'personas_coordinador', 'direccion')


    models = {
        u'personas.barrio': {
            'Meta': {'object_name': 'Barrio'},
            'actualizado': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'ciudad': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['personas.Ciudad']"}),
            'comuna': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'creado': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'personas.cabeza_de_grupo': {
            'Meta': {'object_name': 'Cabeza_de_grupo'},
            'actualizado': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'cedula': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'}),
            'celular': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'coordinador': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['personas.Coordinador']"}),
            'creado': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mail': ('django.db.models.fields.EmailField', [], {'max_length': '50', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'observacion': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'blank': 'True'}),
            'puesto': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['personas.Puesto']", 'blank': 'True'})
        },
        u'personas.ciudad': {
            'Meta': {'object_name': 'Ciudad'},
            'actualizado': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'creado': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'personas.coordinador': {
            'Meta': {'object_name': 'Coordinador'},
            'actualizado': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'cedula': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'}),
            'celular': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'comuna': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'creado': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mail': ('django.db.models.fields.EmailField', [], {'max_length': '50', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'observacion': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'blank': 'True'})
        },
        u'personas.lider': {
            'Meta': {'object_name': 'Lider'},
            'actualizado': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'cabeza_de_grupo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['personas.Cabeza_de_grupo']"}),
            'cedula': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'}),
            'celular': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'creado': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mail': ('django.db.models.fields.EmailField', [], {'max_length': '50', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'observacion': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'blank': 'True'})
        },
        u'personas.puesto': {
            'Meta': {'object_name': 'Puesto'},
            'actualizado': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'ciudad': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['personas.Ciudad']"}),
            'creado': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mesas': ('django.db.models.fields.IntegerField', [], {}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'personas.votante': {
            'Meta': {'object_name': 'Votante'},
            'actualizado': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'barrio': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['personas.Barrio']"}),
            'cedula': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'}),
            'celular': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'creado': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'cumple': ('django.db.models.fields.DateField', [], {'blank': 'True'}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'fijo': ('django.db.models.fields.CharField', [], {'max_length': '8', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lider': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['personas.Lider']"}),
            'mail': ('django.db.models.fields.EmailField', [], {'max_length': '50', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'observacion': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'blank': 'True'}),
            'verificado': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'votacion_ciudad': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'votacion_direccion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'votacion_dpto': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'votacion_inscripcion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'votacion_lugar': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'votacion_mesa': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        }
    }

    complete_apps = ['personas']