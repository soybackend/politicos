# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Lider.votacion_dpto'
        db.add_column(u'personas_lider', 'votacion_dpto',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Adding field 'Lider.votacion_ciudad'
        db.add_column(u'personas_lider', 'votacion_ciudad',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Adding field 'Lider.votacion_lugar'
        db.add_column(u'personas_lider', 'votacion_lugar',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Adding field 'Lider.votacion_direccion'
        db.add_column(u'personas_lider', 'votacion_direccion',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Adding field 'Lider.votacion_inscripcion'
        db.add_column(u'personas_lider', 'votacion_inscripcion',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Adding field 'Lider.votacion_mesa'
        db.add_column(u'personas_lider', 'votacion_mesa',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Lider.votacion_dpto'
        db.delete_column(u'personas_lider', 'votacion_dpto')

        # Deleting field 'Lider.votacion_ciudad'
        db.delete_column(u'personas_lider', 'votacion_ciudad')

        # Deleting field 'Lider.votacion_lugar'
        db.delete_column(u'personas_lider', 'votacion_lugar')

        # Deleting field 'Lider.votacion_direccion'
        db.delete_column(u'personas_lider', 'votacion_direccion')

        # Deleting field 'Lider.votacion_inscripcion'
        db.delete_column(u'personas_lider', 'votacion_inscripcion')

        # Deleting field 'Lider.votacion_mesa'
        db.delete_column(u'personas_lider', 'votacion_mesa')


    models = {
        u'personas.barrio': {
            'Meta': {'object_name': 'Barrio'},
            'actualizado': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'ciudad': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['personas.Ciudad']"}),
            'comuna': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'creado': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'personas.cabeza_de_grupo': {
            'Meta': {'object_name': 'Cabeza_de_grupo'},
            'actualizado': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'candidato_senado': ('django.db.models.fields.related.ForeignKey', [], {'default': "'1'", 'to': u"orm['personas.Candidato_senado']"}),
            'cedula': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'}),
            'celular': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'coordinador': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['personas.Coordinador']"}),
            'creado': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mail': ('django.db.models.fields.EmailField', [], {'max_length': '50', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'observacion': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'blank': 'True'}),
            'puesto': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['personas.Puesto']", 'blank': 'True'})
        },
        u'personas.candidato_senado': {
            'Meta': {'object_name': 'Candidato_senado'},
            'actualizado': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'creado': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'personas.ciudad': {
            'Meta': {'object_name': 'Ciudad'},
            'actualizado': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'creado': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'personas.coordinador': {
            'Meta': {'object_name': 'Coordinador'},
            'actualizado': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'cedula': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'}),
            'celular': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'comuna': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'creado': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mail': ('django.db.models.fields.EmailField', [], {'max_length': '50', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'observacion': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'blank': 'True'})
        },
        u'personas.lider': {
            'Meta': {'object_name': 'Lider'},
            'actualizado': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'cabeza_de_grupo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['personas.Cabeza_de_grupo']"}),
            'candidato_senado': ('django.db.models.fields.related.ForeignKey', [], {'default': "'1'", 'to': u"orm['personas.Candidato_senado']"}),
            'cedula': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'}),
            'celular': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'creado': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mail': ('django.db.models.fields.EmailField', [], {'max_length': '50', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'observacion': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'blank': 'True'}),
            'votacion_ciudad': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'votacion_direccion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'votacion_dpto': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'votacion_inscripcion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'votacion_lugar': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'votacion_mesa': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        u'personas.puesto': {
            'Meta': {'object_name': 'Puesto'},
            'actualizado': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'ciudad': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['personas.Ciudad']"}),
            'creado': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mesas': ('django.db.models.fields.IntegerField', [], {}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'personas.votante': {
            'Meta': {'object_name': 'Votante'},
            'actualizado': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'barrio': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['personas.Barrio']"}),
            'candidato_senado': ('django.db.models.fields.related.ForeignKey', [], {'default': "'1'", 'to': u"orm['personas.Candidato_senado']"}),
            'cedula': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'}),
            'celular': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'creado': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'cumple': ('django.db.models.fields.DateField', [], {'blank': 'True'}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'fijo': ('django.db.models.fields.CharField', [], {'max_length': '8', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lider': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['personas.Lider']"}),
            'mail': ('django.db.models.fields.EmailField', [], {'max_length': '50', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'observacion': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'blank': 'True'}),
            'verificado': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'votacion_ciudad': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'votacion_direccion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'votacion_dpto': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'votacion_inscripcion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'votacion_lugar': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'votacion_mesa': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        }
    }

    complete_apps = ['personas']