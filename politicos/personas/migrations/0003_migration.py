# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Candidato_senado'
        db.create_table(u'personas_candidato_senado', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('creado', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('actualizado', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'personas', ['Candidato_senado'])


    def backwards(self, orm):
        # Deleting model 'Candidato_senado'
        db.delete_table(u'personas_candidato_senado')


    models = {
        u'personas.barrio': {
            'Meta': {'object_name': 'Barrio'},
            'actualizado': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'ciudad': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['personas.Ciudad']"}),
            'comuna': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'creado': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'personas.cabeza_de_grupo': {
            'Meta': {'object_name': 'Cabeza_de_grupo'},
            'actualizado': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'cedula': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'}),
            'celular': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'coordinador': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['personas.Coordinador']"}),
            'creado': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mail': ('django.db.models.fields.EmailField', [], {'max_length': '50', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'observacion': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'blank': 'True'}),
            'puesto': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['personas.Puesto']", 'blank': 'True'})
        },
        u'personas.candidato_senado': {
            'Meta': {'object_name': 'Candidato_senado'},
            'actualizado': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'creado': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'personas.ciudad': {
            'Meta': {'object_name': 'Ciudad'},
            'actualizado': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'creado': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'personas.coordinador': {
            'Meta': {'object_name': 'Coordinador'},
            'actualizado': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'cedula': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'}),
            'celular': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'comuna': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'creado': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mail': ('django.db.models.fields.EmailField', [], {'max_length': '50', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'observacion': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'blank': 'True'})
        },
        u'personas.lider': {
            'Meta': {'object_name': 'Lider'},
            'actualizado': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'cabeza_de_grupo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['personas.Cabeza_de_grupo']"}),
            'cedula': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'}),
            'celular': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'creado': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mail': ('django.db.models.fields.EmailField', [], {'max_length': '50', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'observacion': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'blank': 'True'})
        },
        u'personas.puesto': {
            'Meta': {'object_name': 'Puesto'},
            'actualizado': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'ciudad': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['personas.Ciudad']"}),
            'creado': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mesas': ('django.db.models.fields.IntegerField', [], {}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'personas.votante': {
            'Meta': {'object_name': 'Votante'},
            'actualizado': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'barrio': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['personas.Barrio']"}),
            'cedula': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'}),
            'celular': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'creado': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'cumple': ('django.db.models.fields.DateField', [], {'blank': 'True'}),
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'fijo': ('django.db.models.fields.CharField', [], {'max_length': '8', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lider': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['personas.Lider']"}),
            'mail': ('django.db.models.fields.EmailField', [], {'max_length': '50', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'observacion': ('django.db.models.fields.TextField', [], {'max_length': '1000', 'blank': 'True'}),
            'verificado': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'votacion_ciudad': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'votacion_direccion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'votacion_dpto': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'votacion_inscripcion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'votacion_lugar': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'votacion_mesa': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        }
    }

    complete_apps = ['personas']