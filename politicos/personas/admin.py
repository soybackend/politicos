from django.contrib import admin

from . import models
from actions import export_as_csv


class BarrioAdmin(admin.ModelAdmin):
	list_display = ('nombre', 'comuna', 'ciudad')
	ordering = ('nombre',)
	search_fields = ('nombre', 'comuna')


class VotanteAdmin(admin.ModelAdmin):	
	list_display = ('nombre', 'celular', 'fijo', 'lider', 'barrio', 'votacion_lugar', 'votacion_mesa', 'verificado')	
	list_filter = ('verificado', 'barrio__comuna')
	search_fields = ('cedula', 'nombre', 'lider__nombre','barrio__comuna', 'barrio__nombre')
	list_editable = ('verificado',)
	raw_id_fields = ('barrio','lider')
	ordering = ('nombre',)
	exclude = (
		'verificado',
		'votacion_dpto', 
		'votacion_ciudad', 
		'votacion_lugar', 
		'votacion_direccion', 
		'votacion_inscripcion', 
		'votacion_mesa',
	)
	actions = [export_as_csv]


class CabezaAdmin(admin.ModelAdmin):
	list_display = ('nombre', 'celular', 'puesto', 'coordinador')
	list_filter = ('coordinador', )
	search_fields = ('nombre', 'cedula')
	raw_id_fields = ('puesto', )
	ordering = ('nombre',)
	actions = [export_as_csv]	


class LiderAdmin(admin.ModelAdmin):
	list_display = ('nombre', 'celular', 'cabeza_de_grupo', 'votacion_ciudad', 'votacion_lugar', 'votacion_mesa',)
	search_fields = ('nombre' ,'cedula', 'cabeza_de_grupo__nombre')
	ordering = ('nombre',)
	exclude = (
		'votacion_dpto', 
		'votacion_ciudad', 
		'votacion_direccion', 
		'votacion_inscripcion',
	)
		

class Cabeza_de_grupoInline(admin.StackedInline):
	model = models.Cabeza_de_grupo
	extra = 1


class CoordinadorAdmin(admin.ModelAdmin):
	list_display = ('nombre', 'celular', 'comuna')
	list_filter = ('comuna',)
	search_fields = ('nombre' ,'cedula')
	ordering = ('nombre',)
	inlines = [Cabeza_de_grupoInline]


class PuestoAdmin(admin.ModelAdmin):
	search_fields = ('nombre', 'ciudad')

admin.site.register(models.Candidato_senado)
admin.site.register(models.Coordinador, CoordinadorAdmin)
admin.site.register(models.Cabeza_de_grupo, CabezaAdmin)
admin.site.register(models.Lider, LiderAdmin)
admin.site.register(models.Ciudad)
admin.site.register(models.Puesto, PuestoAdmin)
admin.site.register(models.Barrio, BarrioAdmin)
admin.site.register(models.Votante, VotanteAdmin)