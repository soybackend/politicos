from . import models
from model_report.report import reports, ReportAdmin		

def coordinador_label(report, field):
    return ("Coordinador")

class CabezaReporte(ReportAdmin):
	title = ("Cabezas de Grupo")
	model = models.Cabeza_de_grupo	
	fields = [
		'cedula',
		'nombre',
		'celular',
		'direccion',
		'coordinador__nombre',
		'coordinador__comuna',
		'observacion',		
	]	
	list_order_by = ('coordinador__comuna',)
	list_filter = ('coordinador__nombre',)
	type = 'report'
	override_field_labels = {
        'coordinador__nombre': coordinador_label
    }

reports.register('reporte-cabezas', CabezaReporte)


def cabeza_label(report, field):
    return ("Cabeza de grupo")

def barrio_label(report, field):
    return ("Barrio")

class LiderReporte(ReportAdmin):
	title = ("Lideres")
	model = models.Lider
	fields = [
		'cedula',
		'nombre',
		'celular',
		'direccion',
		'votacion_ciudad',		
		'votacion_lugar',
		'votacion_mesa',
		'cabeza_de_grupo__nombre',
		'cabeza_de_grupo__coordinador__nombre',
		'cabeza_de_grupo__coordinador__comuna',
		'observacion',
	]	
	list_group_by = ('cabeza_de_grupo__nombre',)
	list_filter = ('cabeza_de_grupo__coordinador__nombre','cabeza_de_grupo__nombre',)	
	type = 'report'	
	list_order_by = ('cabeza_de_grupo__coordinador__comuna',)
	override_field_labels = {
        'cabeza_de_grupo__nombre': cabeza_label,
        'cabeza_de_grupo__coordinador__nombre': coordinador_label,
    }
	

reports.register('reporte-lider', LiderReporte)


def lider_label(report, field):
    return ("Lider")	

class VotantesReporte(ReportAdmin):
	title = ("Reporte de Votantes")
	model = models.Votante
	fields = [
		'cedula',
		'nombre',
		'celular',
		'votacion_ciudad',		
		'votacion_lugar',
		'votacion_mesa',
		'barrio__nombre',
		'direccion',
		'lider__nombre',
		'lider__cabeza_de_grupo__coordinador__nombre',
		'lider__cabeza_de_grupo__coordinador__comuna',
		'observacion',
	]
	list_order_by = ('lider__nombre',)
	list_filter = ('lider__cabeza_de_grupo__coordinador__nombre',)
	list_group_by = ('lider__nombre', 'lider__cabeza_de_grupo__coordinador__comuna',)
	list_serie_fields = ('lider__nombre', 'lider__cabeza_de_grupo__coordinador__comuna',)
	type = 'chart'
	chart_types = ('pie', 'column', 'line')
	override_field_labels = {
        'lider__nombre': lider_label,
	'barrio__nombre':barrio_label,
        'lider__cabeza_de_grupo__coordinador__nombre': coordinador_label,
    }

reports.register('reporte-votantes', VotantesReporte)
