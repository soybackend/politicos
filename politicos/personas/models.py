from django.db import models
from django.contrib.auth.models import User
from django.utils.encoding import python_2_unicode_compatible
import urllib2, os, re


class AuditedModel(models.Model):
	creado = models.DateTimeField(auto_now_add=True)
	actualizado = models.DateTimeField(auto_now=True)

	class Meta:
		abstract = True


@python_2_unicode_compatible
class Ciudad(AuditedModel):
	nombre = models.CharField(max_length=100)

	def __str__(self):
		return self.nombre

	class Meta:
		verbose_name_plural = "Ciudades"


@python_2_unicode_compatible
class Puesto(AuditedModel):
	ciudad = models.ForeignKey(Ciudad)
	nombre = models.CharField(max_length=100)
	direccion = models.CharField(max_length=100)
	mesas = models.IntegerField()

	def __str__(self):
		return self.nombre


@python_2_unicode_compatible
class Barrio(AuditedModel):
	nombre = models.CharField(max_length=100)
	comuna = models.CharField(max_length=1)
	ciudad = models.ForeignKey(Ciudad)

	def __str__(self):
		return self.nombre


@python_2_unicode_compatible
class Coordinador(AuditedModel):
	cedula = models.CharField(max_length=10, unique=True)
	nombre = models.CharField(max_length=100)
	celular = models.CharField(max_length=10, blank=True)
	direccion = models.CharField(max_length=100, blank=True)
	mail = models.EmailField(max_length=50, blank=True)
	comuna = models.CharField(max_length=1)	
	observacion = models.TextField(max_length=1000, blank=True)

	def __str__(self):
		return self.nombre

	class Meta:
		verbose_name_plural = "Coordinadores"


@python_2_unicode_compatible
class Candidato_senado(AuditedModel):
	nombre = models.CharField(max_length=50)

	def __str__(self):
		return self.nombre

	class Meta:
		verbose_name_plural = "Candidatos al senado"


@python_2_unicode_compatible
class Cabeza_de_grupo(AuditedModel):
	cedula = models.CharField(max_length=10, unique=True)
	nombre = models.CharField(max_length=100)
	celular = models.CharField(max_length=10, blank=True)
	direccion = models.CharField(max_length=100, blank=True)
	mail = models.EmailField(max_length=50, blank=True)	
	coordinador = models.ForeignKey(Coordinador)
	candidato_senado = models.ForeignKey(Candidato_senado, default="1")
	puesto = models.ForeignKey(Puesto, blank=True)	
	observacion = models.TextField(max_length=1000, blank=True)

	def __str__(self):
		return self.nombre

	class Meta:
		verbose_name_plural = "Cabezas de grupo"


@python_2_unicode_compatible
class Lider(AuditedModel):
	cedula = models.CharField(max_length=10, unique=True)
	nombre = models.CharField(max_length=100)	
	celular = models.CharField(max_length=10, blank=True)
	direccion = models.CharField(max_length=100, blank=True)
	mail = models.EmailField(max_length=50, blank=True)	
	cabeza_de_grupo = models.ForeignKey(Cabeza_de_grupo)	
	candidato_senado = models.ForeignKey(Candidato_senado, default="1")
	observacion = models.TextField(max_length=1000, blank=True)

	votacion_dpto = models.CharField(max_length=100, blank=True)
	votacion_ciudad = models.CharField(max_length=100, blank=True)
	votacion_lugar = models.CharField(max_length=100, blank=True)
	votacion_direccion = models.CharField(max_length=100, blank=True)
	votacion_inscripcion = models.CharField(max_length=100, blank=True)
	votacion_mesa = models.CharField(max_length=100, blank=True)

	def save(self, *args, **kwargs):
		
		if (self.id==None):
			self.votacion_dpto = 'No registra'
			self.votacion_ciudad = 'No registra' 
			self.votacion_lugar = 'No registra'
			self.votacion_direccion = 'No registra'
			self.votacion_inscripcion = 'No registra'
			self.votacion_mesa = 'No registra'
		
		super(Lider, self).save(*args, **kwargs)

	def __str__(self):
		return self.nombre

	class Meta:
		verbose_name_plural = "Lideres"
		

@python_2_unicode_compatible
class Votante(AuditedModel):
	lider = models.ForeignKey(Lider)	
	nombre = models.CharField(max_length=80)
	cedula = models.CharField(max_length=10, unique=True)
	celular = models.CharField(max_length=10, blank=True)
	direccion = models.CharField(max_length=100, blank=True)
	mail = models.EmailField(max_length=50, blank=True)	
	fijo = models.CharField(max_length=8, blank=True)
	barrio = models.ForeignKey(Barrio)
	cumple = models.DateField(help_text='Ej: 07/12/1988', blank=True)
	verificado = models.BooleanField(default=False)	
	candidato_senado = models.ForeignKey(Candidato_senado, default="1")
	observacion = models.TextField(max_length=1000, blank=True)

	votacion_dpto = models.CharField(max_length=100, blank=True)
	votacion_ciudad = models.CharField(max_length=100, blank=True)
	votacion_lugar = models.CharField(max_length=100, blank=True)
	votacion_direccion = models.CharField(max_length=100, blank=True)
	votacion_inscripcion = models.CharField(max_length=100, blank=True)
	votacion_mesa = models.CharField(max_length=100, blank=True)
 
	def save(self, *args, **kwargs):
	# 	webresult = urllib2.urlopen("http://www3.registraduria.gov.co/censo/_censoresultado.php?nCedula="+self.cedula)
	# 	text = webresult.read().decode('iso-8859-1')
	# 	data = text[text.find('<table width="580" align="center" cellpadding="0" cellspacing="1">'): text.find("</table>")]
	# 	data = data.encode('utf8')
	# 	list = re.findall(r'<[^>]*>', data)
	# 	for key in list:
	# 		data = data.replace(key,'')
	# 	split = data.split('\n')
	# 	datos = []
	# 	for pos in range(len(split)):
	# 		if pos % 2 != 0 and split[pos].strip() !='':
	# 			datos.append(split[pos].strip())
	# 	if len(datos) >= 6:
	# 		self.votacion_dpto = datos[0]
	# 		self.votacion_ciudad = datos[1]
	# 		self.votacion_lugar = datos[2]
	# 		self.votacion_direccion = datos[3]
	# 		self.votacion_inscripcion = datos[4]
	# 		self.votacion_mesa = datos[5] 
	# 	else:
		if (self.id==None):
			self.votacion_dpto = 'No registra'
			self.votacion_ciudad = 'No registra' 
			self.votacion_lugar = 'No registra'
			self.votacion_direccion = 'No registra'
			self.votacion_inscripcion = 'No registra'
			self.votacion_mesa = 'No registra'
	# 	webresult.close()
		super(Votante, self).save(*args, **kwargs)

	def __str__(self):
		return self.nombre
