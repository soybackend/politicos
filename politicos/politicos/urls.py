from django.conf.urls import patterns, include, url

from django.contrib import admin
from model_report import report

admin.autodiscover() 
report.autodiscover()

urlpatterns = patterns(
	'',    
	url(r'^reportes/', include('model_report.urls')),
    url(r'^', include(admin.site.urls)),        
)
