import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

SECRET_KEY = '4ru=@i9^d6bc03^mr%80-)r@xrg%4*udxzx$mzps4q&lgr7baw'

DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['politicos.josedeweb.com']

INSTALLED_APPS = (
    'gunicorn',  #deploy     
    'model_report',
    'django_admin_bootstrapped', #bootstrap    
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'personas', #app que yo cree
    'south',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'politicos.urls'

WSGI_APPLICATION = 'politicos.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'lucycontento',
        'USER': 'admin',    
        'PASSWORD': 'l8cyc0nt3nt0',
        'HOST': '127.0.0.1',
        'PORT': '3306',
    }
}

LANGUAGE_CODE = 'es'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'

STATIC_ROOT = BASE_DIR+'/static/'

TEMPLATE_DIRS = BASE_DIR + '/templates/'