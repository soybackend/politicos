Django==1.6.1
MySQL-python==1.2.5
django-admin-bootstrapped==1.6.3
gunicorn==18.0
South==0.8.4
django-model-report==0.1.7